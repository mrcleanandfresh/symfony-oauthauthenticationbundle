<?php
/**
 * OAuth1.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

namespace OAuth\AuthenticationBundle\Version\OAuth1;

use OAuth\AuthenticationBundle\Version\Excep\OAuth1Exception;


/**
 * Class OAuth1
 * @package AppBundle\Service\Authentication\OAuth
 */
class OAuth1
{
	/**
	 * OAuth Version
	 * @var string
	 */
	public static $version = "1.0";

	/**
	 * OAuth Signature Method
	 * @var string
	 */
	public static $signature_method = "HMAC-SHA1";

	/**
	 * OAuth Consumer Key
	 * @var (string)
	 */
	private $consumer_key;

	/**
	 * OAuth Consumer Secret
	 * @var (string)
	 */
	private $consumer_secret;

	/**
	 * OAuth Token
	 * @var (string)
	 */
	private $token;

	/**
	 * OAuth Token Secret
	 * @var (string)
	 */
	private $token_secret;

	/**
	 * Simple constructor
	 * @param (string) $consumer_key Will be set to the instance property of $consumer_key
	 * @param (string) $token        Will be set to the instance property of $token
	 */
	public function __construct($consumer_key, $consumer_secret, $token, $token_secret) {
		$this->consumer_key = $consumer_key;
		$this->consumer_secret = $consumer_secret;
		$this->token = $token;
		$this->token_secret = $token_secret;
	}

	/**
	 *
	 * Creates a unique identifier. Using Preg_Replace we strip out all non-word characters and base64 encode 32 random bites of data.
	 * @return mixed Returns the nonce string
	 */
	public static function createNonce() {
		$nonce = preg_replace('/[^\da-z]/i', '', base64_encode(openssl_random_pseudo_bytes(32)));
		return $nonce;
	}

	/**
	 *
	 * Creates a time stamp using PHP's time function which is the current time measured in the number of seconds since Unix Epoch
	 * @return int Returns time since Unix Epoch
	 */
	public static function createTimeStamp() {
		$timestamp = time();
		return $timestamp;
	}

	public function encodeParameterKeys(array $params) {
		// Percent encode every key and value that will be signed.
		$encodedParams = array();
		foreach ($params as $key => $parameter) {
			$encodedParams[rawurlencode($key)] = rawurlencode($parameter);
		}
		return $encodedParams;
	}

	public function sortParametersAlphaByKey($params) {
		// Sort the list of parameters alphabetically[1] by encoded key[2].
		if(!empty($params)) {
			ksort($params);
		}
		return $params;
	}

	/**
	 * @param string $request_type
	 * @param $url
	 * @param array $signature_parameters
	 * @return string
	 */
	public function buildSignatureBaseString($request_type = "POST", $url, array $signature_parameters) {
		if (null === $request_type) $request_type = "POST";

		$encoded_signature_parameters = array();

		// Percent encode every key and value that will be signed.
		foreach ($signature_parameters as $key => $parameter) {
			$encoded_signature_parameters[rawurlencode($key)] = rawurlencode($parameter);
		}

		// Sort the list of parameters alphabetically[1] by encoded key[2].
		if(!empty($encoded_signature_parameters)) {
			ksort($encoded_signature_parameters);
		}

		$parameter_string = array();

		foreach ($encoded_signature_parameters as $key => $value) {
			//Encode the key, append "=", append value. 
			$parameter_string[] = "{$key}={$value}";
		}
		
		//Ampersand deliminator.
		$signature_base_string = strtoupper($request_type) . "&" . rawurlencode($url) . "&" . rawurlencode(implode('&', $parameter_string));

		return $signature_base_string;
	}

	/**
	 *
	 * Creates the signature used for OAuth
	 * @param $nonce
	 * @param $timestamp
	 * @param $request_type
	 * @param $url
	 * @param array $request_parameters
	 * @return string (string)        OAuth Signature.
	 * @throws OAuth1Exception
	 */
	public function signature($nonce, $timestamp, $request_type, $url, array $request_parameters) {
		if(self::$signature_method !== "HMAC-SHA1") {
			throw new OAuth1Exception("Must use HMAC-SHA1 to access OAuth.", 1);
		}

		$signature_parameters = array(
				'oauth_consumer_key' 	=> $this->consumer_key,
				'oauth_nonce' 			=> $nonce,
				'oauth_signature_method' => self::$signature_method,
				'oauth_timestamp' 		=> $timestamp,
				'oauth_token' 			=> $this->token,
				'oauth_version' 		=> self::$version
			) + $request_parameters;

		$signature_base_string = $this->buildSignatureBaseString($request_type, $url, $signature_parameters);

		//The signing key is an encoded consumer secret followed by an "&" and encoded token secret
		$signing_key = rawurlencode($this->consumer_secret) . "&" . rawurlencode($this->token_secret);

		$signature = base64_encode(hash_hmac("sha1", $signature_base_string, $signing_key, true));

		return $signature;
	}

	/**
	 *
	 *    Build the header string
	 *
	 * @example OAuth oauth_consumer_key="xvz1evFS4wEEPTGEFPHBog", oauth_nonce="kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg", oauth_signature="tnnArxj06cWHq44gCs1OSKk%2FjLY%3D", oauth_signature_method="HMAC-SHA1", oauth_timestamp="1318622958", oauth_token="370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb", oauth_version="1.0"
	 * @param $request_type
	 * @param $url
	 * @param array $request_parameters
	 * @return string (array)
	 */
	public function buildOAuthHeader($request_type, $url, array $request_parameters) {

		$nonce = self::createNonce();
		$timestamp = self::createTimeStamp();

		$signature = $this->signature($nonce, $timestamp, $request_type, $url, $request_parameters);

		/**
		 *	Append the string "OAuth " (including the space at the end) to $header
		 */
		$header = "Authorization: OAuth ";

		//	Collect parameters
		$parameters = array(
			'oauth_consumer_key' => $this->consumer_key,
			'oauth_nonce' => $nonce,
			'oauth_signature' => $signature,
			'oauth_signature_method' => self::$signature_method,
			'oauth_timestamp' => $timestamp,
			'oauth_token' => $this->token,
			'oauth_version' => self::$version
		);

		foreach ($parameters as $key => $parameter) {
			//Encode the key, append "=", append " then encode value, and append " and comma deliminate
			$header .= rawurlencode($key) . "=\"" . rawurlencode($parameter) ."\"";
			if($parameter !== end($parameters)) {
				$header .= ", ";
			}
		}

		return $header;
	}
}