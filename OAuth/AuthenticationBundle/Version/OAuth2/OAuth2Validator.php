<?php
/**
 * OAuth2Validator.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 5/27/2016
 * Time: 9:56 AM
 */

namespace OAuth\AuthenticationBundle\Version\OAuth2;


use OAuth\AuthenticationBundle\Version\Excep\OAuth2Exception;

class OAuth2Validator
{
	/**
	 * Check Whether the State Matches
	 * @param $requestState
	 * @param $returnedState
	 * @return bool
	 * @throws OAuth2Exception
	 */
	public static function checkState(OAuth2Flow $requestState, $returnedState) {
		if($returnedState === $requestState) {
			return true;
		} else {
			throw new OAuth2Exception("The States do not match! Possible XSS attack");
		}
	}

	/**
	 * Check Whether the Scopes Match
	 * @param $requestedScopes
	 * @param $returnedScopes
	 * @return bool
	 */
	public static function checkScopes($requestedScopes, $returnedScopes) {
		if(is_array($returnedScopes)) {
			$compare = array_diff($requestedScopes, $returnedScopes);
		} else {
			if(is_array($requestedScopes)) {
				return false;
			} else {
				$compare = strcmp($requestedScopes, $returnedScopes);
			}
		}

		/*
		 * array_diff returns an array of differences, if the array is empty, there are no differences
		 * strcmp returns a 0 if they are equal, meaning no differences
		 * In either case, we want to return true because there were no differences in scopes.
		 */
		if(empty($compare) || 0 === $compare) {
			return true;
		} else {
			return false;
		}
	}

	public static function getInstance() {
		return new self;
	}
}