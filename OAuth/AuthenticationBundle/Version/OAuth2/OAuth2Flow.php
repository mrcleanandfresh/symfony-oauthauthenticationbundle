<?php
/**
 * OAuth2Flow.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

namespace OAuth\AuthenticationBundle\Version\OAuth2;

use OAuth\AuthenticationBundle\Version\Excep\OAuth2Exception;
use OAuth\AuthenticationBundle\Version\HTTPRequestService;

/**
 * Class OAuth2Flow
 * @package AppBundle\Service\Authentication\OAuth\OAuth2
 */
abstract class OAuth2Flow
{
	/**
	 * Instance of the OAuth2Config
	 * @var OAuth2Config $config
	 */
	protected $config;
	
	/**
	 * The state parameter sent in the request
	 * @var $state
	 */
	protected $state;

	/**
	 * The authorization code to exchange for tokens
	 * @var $authorization_code
	 */
	protected $authorization_code;

	/**
	 * The access token received from a successful request
	 * @var $access_token
	 */
	protected $access_token;

	/**
	 * The refresh token, optionally received from a successful request
	 * @var $refresh_token
	 */
	protected $refresh_token;

	/**
	 * The type of the token we are requesting
	 * @var $token_type
	 */
	protected $token_type;

	/**
	 * The length of time we have before the token expires
	 * @var $expires_in
	 */
	protected $time_to_expire;

	/**
	 * The scope or scopes that we are requesting of the authentication server
	 * @var array $scope
	 */
	protected $scope = array();

	/**
	 * Shared authenticate() method
	 * @return mixed
	 */
	abstract public function authenticate();

	protected function captureRedirection() {}

	/**
	 * Add a new Scope
	 * @param $scope
	 * @return $this
	 */
	public function addScope($scope) {
		if(is_array($scope)) {
			foreach ($scope as $single_scope) {
				array_push($this->scope, $single_scope);
			}
		} else {
			array_push($this->scope, $scope);
		}

		return $this;
	}

	/**
	 * Check if it's valid Json
	 *
	 * @param $string
	 * @return bool
	 */
	public static function isValidJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	/**
	 * Handle what the token response returns
	 *
	 * @param $response
	 *
	 * @return $this
	 * @throws OAuth2Exception
	 */
	public function handleTokenResponse($response) {
		if(!self::isValidJson($response)) {
			// If it's a success, but it returns HTML, we want to let the user know.
			if(preg_match('/<h1>([^<\\/h1>]*)<\\/h1>/', $response, $match)) {
				throw new OAuth2Exception("<em>The request was an apparent success, but a message was sent back: ".strip_tags($match[0])."</em>");
			}
		}

		$response_obj = json_decode($response);

		if(is_object($response_obj)) {
			if (property_exists($response_obj, 'error')) {

				$this->handleErrorResponse($response_obj->error);

			} elseif (property_exists($response_obj, 'access_token')) {

				$this->setAccessToken($response_obj->access_token);

				(property_exists($response_obj, 'refresh_token'))
					? $this->setRefreshToken($response_obj->refresh_token)
					: $this->setRefreshToken(null);

				//add time() to expire to current time
				$this->setTimeToExpire($response_obj->expires_in + time());
			}
		}

		return $this;
	}

	/**
	 * Exchange authentication information for tokens, depending on grant_type
	 * @return $this
	 * @throws OAuth2Exception
	 */
	public function refreshTokens() {
		$config = $this->getConfig();
		$url = $config->getTokenUrl();

		$parameters = array(
			'client_secret'	=> $this->getConfig()->getClientSecret(),
			'grant_type'	=> $config::REFRESH_TOKEN,
			'refresh_token'	=> $this->getRefreshToken(),
			'client_id'		=> $this->getConfig()->getClientId(),
		);

		$addl_param = array(
			CURLOPT_POST			=> true,
			CURLOPT_POSTFIELDS		=> http_build_query($parameters)
		);

		$payload = HTTPRequestService::performRequest($url, $config::IS_SSL_ENABLED, $addl_param);

		$this->handleTokenResponse($payload);

		return $this;
	}

	/**
	 * Handle Errors in the Response we receive back
	 * @param $response
	 * @throws OAuth2Exception
	 */
	public function handleErrorResponse($response) {
		if (is_object($response) && property_exists($response, 'error')) {
			switch ($response->error) {
				case "invalid_request" :
					throw new OAuth2Exception("Invalid Request. The request is missing a required parameter, includes an invalid parameter value, includes a parameter more than once, or is otherwise malformed. ".$response->error_description);
					break;
				case "unauthorized_client" :
					throw new OAuth2Exception("Unauthorized Client. The client is not authorized to request an authorization code using this method.");
					break;
				case "access_denied" :
					throw new OAuth2Exception("Access is Denied. The client is not authorized to request an authorization code using this method.");
					break;
				case "unsupported_response_type" :
					throw new OAuth2Exception("Unsupported Response Type. The authorization server does not support obtaining an authorization code using this method.");
					break;
				case "invalid_scope" :
					throw new OAuth2Exception("Invalid Scope. The requested scope is invalid, unknown or malformed.");
					break;
				case "server_error" :
					throw new OAuth2Exception("500 Server Error. The authorization server encountered an unexpected condition that prevented it from fulfilling the request.");
					break;
				case "temporarily_unavailable" :
					throw new OAuth2Exception("503 Temporarily Unavailable. The authorization server is currently unable to handle the request due to a temporary overloading or maintenance of the server.");
					break;
				default :
					if(isset($response->error_description)) {
						(preg_match("/\\.$/", $response->error)) ? $error = $response->error : $error = $response->error.".";
						(preg_match("/\\.$/", $response->error_description)) ? $description = $response->error_description : $description = $response->error_description.".";
						throw new OAuth2Exception($error." ".$description);
					} else {
						(preg_match("/\\.$/", $response->error)) ? $error = $response->error : $error = $response->error.".";
						throw new OAuth2Exception($error);
					}
			}
		}
	}

	/**
	 * @return OAuth2Config
	 */
	public function getConfig() {
		return $this->config;
	}

	/**
	 * @param OAuth2Config $config
	 */
	protected function setConfig($config) {
		$this->config = $config;
	}

	/**
	 * @return mixed
	 */
	public function getScope()
	{
		$scopes = $this->scope;
		if (is_array($scopes) && !empty($scopes)) {
			$scope_list = implode(" ", $scopes);
		} else {
			$scope_list = $scopes;
		}
		return $scope_list;
	}

	/**
	 * @param mixed $scope
	 *
	 * @return $this
	 */
	public function setScope($scope)
	{
		$this->scope = $scope;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getState()
	{
		return $this->state;
	}

	/**
	 * @param mixed $state
	 *
	 * @return $this
	 */
	public function setState($state)
	{
		$this->state = $state;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getAccessToken()
	{
		return $this->access_token;
	}

	/**
	 * @param mixed $access_token
	 */
	public function setAccessToken($access_token)
	{
		$this->access_token = $access_token;
	}

	/**
	 * @return mixed
	 */
	public function getRefreshToken()
	{
		return $this->refresh_token;
	}

	/**
	 * @param mixed $refresh_token
	 */
	public function setRefreshToken($refresh_token)
	{
		$this->refresh_token = $refresh_token;
	}

	/**
	 * @return mixed
	 */
	public function getTimeToExpire()
	{
		return $this->time_to_expire;
	}

	/**
	 * @param mixed $time_to_expire
	 *
	 * @return $this
	 */
	public function setTimeToExpire($time_to_expire)
	{
		$this->time_to_expire = $time_to_expire;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getAuthorizationCode() {
		return $this->authorization_code;
	}

	/**
	 * @param mixed $authorization_code
	 *
	 * @return $this
	 */
	public function setAuthorizationCode($authorization_code) {
		$this->authorization_code = $authorization_code;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTokenType() {
		return $this->token_type;
	}

	/**
	 * @param mixed $token_type
	 *
	 * @return $this
	 */
	public function setTokenType($token_type) {
		$this->token_type = $token_type;

		return $this;
	}
	
	
}