<?php
/**
 * RedirectGrant.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

namespace OAuth\AuthenticationBundle\Version\OAuth2\Impl;

interface RedirectGrant
{
	public function handleAccessTokenResponse($response);
	public function getGrantParameters(array $scopes);
	public function buildOAuthURL($scopes);
}