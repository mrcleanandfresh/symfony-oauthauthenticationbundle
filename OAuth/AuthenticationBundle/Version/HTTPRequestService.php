<?php
/**
 * HTTPRequestService.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

namespace OAuth\AuthenticationBundle\Version;

/**
 * Class HTTPRequestService
 * @package Authentication\OAuth
 */
class HTTPRequestService {

	/**
	 * @param            $url
	 * @param            $hasSSL
	 * @param array|null $addl_options
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public static final function performRequest($url, $hasSSL, array $addl_options = null) {
		$ch = curl_init();

//		if (!empty($addl_options[CURLOPT_HTTPHEADER])) {
//			array_push($addl_options[CURLOPT_HTTPHEADER], "Authorization: Basic ".base64_encode("kemet:charged"));
//		} else {
//			$addl_options[CURLOPT_HTTPHEADER] = array(
//				"authorization: Basic ".base64_encode("kemet:charged").""
//			);
//		}

		$options = array(
			CURLOPT_CONNECTTIMEOUT 	=> 5,
			CURLOPT_TIMEOUT 		=> 30,
			CURLOPT_URL				=> $url,
			CURLOPT_RETURNTRANSFER 	=> true,
		) + $addl_options;

		if ($hasSSL) {
			$options[CURLOPT_PROTOCOLS] 		= CURLPROTO_HTTPS;
			$options[CURLOPT_SSL_VERIFYHOST] 	= 2;
			$options[CURLOPT_RETURNTRANSFER] 	= 1;
		}

		curl_setopt_array($ch, $options);
		$payload = curl_exec($ch);

		if (false === $payload) {
			$humanReadableError = curl_error($ch);
			$errorInfo = curl_getinfo($ch);

			curl_close($ch);

			throw new \Exception("cURL failed to get OAuth Tokens. ".$humanReadableError.". We tried for ". $errorInfo['total_time'] ." seconds to call \"". $errorInfo['url']."\"\n");
		}

		curl_close($ch);

		return $payload;
	}

	/**
	 * @param array $http_options
	 * @param       $url
	 * @param       $hasSSL
	 * @param null  $access_token
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public static final function performResourceRequest(array $http_options, $url, $hasSSL, $access_token = null) {
		if (null === $access_token)
			throw new \Exception("Trying to request a resource with a null Access Token! Can't do that.");

		(isset($http_options['headers'])) ? $headers = $http_options['headers'] : $headers = array();

		$extra_options = array();

		if('POST' == strtoupper($http_options['method'])) {
			if (null !== $access_token) {
				$url .= "?access_token=".$access_token;
			}
			
			$post_parameters = json_encode($http_options['data']);

			$extra_options[CURLOPT_POST] = true;
			$extra_options[CURLOPT_HTTPHEADER] = $headers;
			$extra_options[CURLOPT_CUSTOMREQUEST] = "POST";
			$extra_options[CURLOPT_POSTFIELDS] = $post_parameters;
		} else if ('GET' == strtoupper($http_options['method'])) {
			$query_parameters = $http_options['data'];
			// Main URL set in the performRequest Method
			$url .= $query_parameters;
			$extra_options[CURLOPT_HTTPGET] = true;
		}

		return self::performRequest($url, $hasSSL, $extra_options);
	}
}