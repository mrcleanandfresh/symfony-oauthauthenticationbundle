<?php
/**
 * HomeController.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

namespace OAuth\AuthenticationBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{
	/**
	 * @Template("@OAuthAuthentication/Tests/auth-code-test.html.twig")
	 */
	public function homeAction(Request $request) {
		$grants = array(
			'https://www.googleapis.com/auth/plus.login',
			'https://www.googleapis.com/auth/plus.me',
			'https://www.googleapis.com/auth/userinfo.email',
			'https://www.googleapis.com/auth/userinfo.profile',
		);

		$this->get('oauth.google.code_grant')->setScope($grants);
		
		return array(
			'oauthURL'	=> $this->get('oauth.google.code_grant')->buildOAuthURL($grants)
		);
	}

	public function authenticatedAction(Request $request) {
		$object = $this->get('oauth.google.code_grant');

		return new JsonResponse($object);
	}
}
