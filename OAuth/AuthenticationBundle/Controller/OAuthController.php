<?php
/**
 * OAuthController.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

namespace OAuth\AuthenticationBundle\Controller;

use OAuth\AuthenticationBundle\Version\HTTPRequestService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class OAuthController
 * @package OAuth\AuthenticationBundle\Controller
 * @Route("/oauth")
 */
class OAuthController extends Controller
{
    /**
     * @Route("/redirect", name="o_auth_authentication_redirect")
	 * @Template("@OAuthAuthentication/Tests/auth-code-test.html.twig")
     */
    public function redirectAction(Request $request) {
		// problem is, when PHP destroys itself between requests. So when a new request is made, these aren't saved.
		$this->get('oauth.google.code_grant')->setState($request->query->get('state'));
		$this->get('oauth.google.code_grant')->setCode($request->query->get('code'));

		return array(
			'state'	=> $request->query->get('state'),
			'code'	=> $request->query->get('code')
		);
    }

	/**
	 * @Route("/oauth2callback", name="o_auth_authentication_oauth2callback")
	 */
	public function callbackAction(Request $request) {
		$request_arr = $request->request->all();

		$request_arr['client_id'] = $this->get('oauth.google.code_grant')->getConfig()->getClientId();
		$request_arr['client_secret'] = $this->get('oauth.google.code_grant')->getConfig()->getClientSecret();
		$request_arr['grant_type']	= "authorization_code";

		$ch = curl_init();
		//make POST request to token url, url-encoded.

		$options = array();

		$options[CURLOPT_POST] = true;
		$options[CURLOPT_HTTPHEADER] = array("content-type: application/x-www-form-urlencoded");
		$options[CURLOPT_CUSTOMREQUEST] = "POST";
		$options[CURLOPT_POSTFIELDS] = http_build_query($request_arr);
		$options[CURLOPT_CONNECTTIMEOUT] 	= 5;
		$options[CURLOPT_TIMEOUT] 		= 30;
		$options[CURLOPT_URL]			= "https://www.googleapis.com/oauth2/v4/token";
		$options[CURLOPT_RETURNTRANSFER]	= true;

		curl_setopt_array($ch, $options);
		$payload = curl_exec($ch);

		if (false === $payload) {
			$humanReadableError = curl_error($ch);
			$errorInfo = curl_getinfo($ch);

			curl_close($ch);

			throw new \Exception("cURL failed to get OAuth Tokens. ".$humanReadableError.". We tried for ". $errorInfo['total_time'] ." seconds to call \"". $errorInfo['url']."\"\n");
		}

		curl_close($ch);

		//save resulting access token and refresh token.
		$this->get('oauth.google.code_grant')->handleTokenResponse($payload);

		//return user to home page.
		return $this->redirectToRoute('o_auth_authentication_authenticated');
	}
}
