<?php
/**
 * OAuthAuthenticationExtension.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

namespace OAuth\AuthenticationBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class OAuthAuthenticationExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        if (isset($config['providers'])) {
            if (isset($config['configuration'])) {
                $container->setParameter('response_type', $config['configuration']['response_type_default']);
            }
            if (isset($config['configuration']['urls'])) {
				$container->setParameter('redirect_uri', $config['configuration']['urls']['redirect_uri']);
                foreach ($config['configuration']['urls']['providers'] as $provider => $settings) {
                    if (null !== $settings && !empty($settings)) {
                        foreach ($settings as $url_type => $url) {
                            $container->setParameter($provider.".".$url_type, $url);
                        }
                    }
                }
            }
            
            foreach ($config['providers'] as $provider => $settings) {
                foreach ($settings as $setting_key => $setting_value) {
                    //Automatically creates a parameter key of "provider.foo_key = value"
                    $container->setParameter($provider.".".$setting_key, $setting_value);
                }
            }
        }
    }
}
