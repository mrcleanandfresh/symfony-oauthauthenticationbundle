<?php
/**
 * Configuration.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

namespace OAuth\AuthenticationBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('o_auth_authentication');

        $rootNode
            ->children()
				->arrayNode('configuration')
					->children()
						->arrayNode('urls')
							->children()
								->scalarNode('redirect_uri')->end()
								->arrayNode('providers')
									->children()
										->arrayNode('twitter')
			 								->beforeNormalization()
											->ifNull()
												->then(function() { return array(); })
											->end()
											->children()
												->scalarNode('authorization_url')->defaultNull()->end()
												->scalarNode('token_url')->defaultNull()->end()
											->end()
										->end()
										->arrayNode('google_plus')
											->beforeNormalization()
											->ifNull()
												->then(function() { return array(); })
											->end()
											->children()
												->scalarNode('authorization_url')->defaultNull()->end()
												->scalarNode('token_url')->defaultNull()->end()
											->end()
										->end()
									    ->arrayNode('facebook')
											->beforeNormalization()
											->ifNull()
												->then(function() { return array(); })
											->end()
											->children()
												->scalarNode('authorization_url')->defaultNull()->end()
												->scalarNode('token_url')->defaultNull()->end()
											->end()
										->end()
									    ->arrayNode('github')
											->beforeNormalization()
											->ifNull()
												->then(function() { return array(); })
											->end()
											->children()
												->scalarNode('authorization_url')->defaultNull()->end()
												->scalarNode('token_url')->defaultNull()->end()
											->end()
										->end()
										->arrayNode('component_edge')
											->beforeNormalization()
											->ifNull()
												->then(function() { return array(); })
											->end()
											->children()
												->scalarNode('authorization_url')->defaultNull()->end()
												->scalarNode('token_url')->defaultNull()->end()
											->end()
										->end()
									->end()
								->end()
							->end()
						->end()
						->scalarNode('response_type_default')->end()
					->end()
				->end()
				->arrayNode('providers')
					->children()
						->arrayNode('twitter')
							->children()
								->scalarNode('consumer_key')->end()
								->scalarNode('consumer_secret')->end()
								->scalarNode('access_token')->end()
								->scalarNode('access_token_secret')->end()
							->end()
						->end() //Twitter
						->arrayNode('google_plus')
							->children()
								->scalarNode('client_id')->end()
								->scalarNode('client_secret')->end()
							->end()
						->end()
						->arrayNode('facebook')
							->children()
								->scalarNode('app_id')->end()
								->scalarNode('app_secret')->end()
							->end()
						->end()
						->arrayNode('github')
							->children()
								->scalarNode('client_id')->end()
								->scalarNode('client_secret')->end()
							->end()
						->end()
						->arrayNode('component_edge')
							->children()
								->scalarNode('client_id')->end()
								->scalarNode('client_secret')->end()
								->scalarNode('username')->end()
								->scalarNode('password')->end()
							->end()
						->end()
					->end()
				->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
