<?php
/**
 * RestAPI.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */


namespace OAuth\AuthenticationBundle\Impl;


interface RestAPI
{
	function setGETParameters($query);
	function setPOSTParameters($request);
	static function isValidJson($string);
}