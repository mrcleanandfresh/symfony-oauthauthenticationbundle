<?php
/**
 * OAuth2Connection.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

namespace OAuth\AuthenticationBundle;

use OAuth\AuthenticationBundle\Impl\RestAPI;
use OAuth\AuthenticationBundle\Version\Excep\OAuth2Exception;
use OAuth\AuthenticationBundle\Version\HTTPRequestService;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Config;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Flow;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class Connection
 * @package AppBundle\Service\Authentication
 */
class OAuth2Connection implements RestAPI
{
	/**
	 * Instance of the Config Object
	 * @var OAuth2Config $config
	 */
	protected $config;

	/**
	 * Instance of the Symfony container
	 * @var Container $container
	 */
	protected $container;

	/**
	 * Object of single|multiple grants
	 * @var OAuth2Flow $grant
	 */
	protected $grant;

	/**
	 * OAuth2Connection constructor.
	 *
	 * @param OAuth2Config $config
	 * @param OAuth2Flow $grant
	 */
	public function __construct(OAuth2Config $config, OAuth2Flow $grant)
	{
		$this->setGrantAndAuthenticate($grant);
		$this->setConfig($config);

		$this->container = new Container();
	}

	/**
	 * Build a GET request from the query string
	 * 
	 * @param $query
	 * @return string
	 */
	public function setGETParameters($query) {
		if(!empty($query)) {
			$query_array = array();
			foreach ($query as $parameter => $value)
			{
				if ($parameter !== '')
				{
					$query_array[$parameter] = $value;
				}
			}

			error_log("?access_token=" . $this->getGrant()->getAccessToken() . "&" . http_build_query($query_array));
			return "?access_token=" . $this->getGrant()->getAccessToken() . "&" . http_build_query($query_array);
		} else {
			return "?access_token=" . $this->getGrant()->getAccessToken();
		}
	}

	/**
	 * Build POST parameters
	 * 
	 * @param $request
	 * @return mixed
	 */
	public function setPOSTParameters($request) {
		// Essentially, if a value is a boolean, we want to change it to a string.
		foreach ($request as $key => &$value)
		{
			if (is_bool($value))
			{
				$value = ($value === true) ? 'true' : 'false';
			}
		}
		return $request;
	}

	/**
	 * Check if it's valid Json
	 * 
	 * @param $string
	 * @return bool
	 */
	public static function isValidJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	/**
	 * Perform the API Request
	 *
	 * @param $access_token
	 * @param $url
	 * @param $http_method
	 * @param $data
	 *
	 * @return mixed
	 * @throws OAuth2Exception
	 */
	public function requestResource($access_token, $url, $http_method, $data) {
		if ('POST' === strtoupper($http_method)) {
			$formatted_data = $this->setPOSTParameters($data);
		} elseif ('GET' === strtoupper($http_method)) {
			$formatted_data = $this->setGETParameters($data);
		} else {
			$formatted_data = null;
		}
		
		$config = $this->getConfig();

		$headers = array(
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$http_options = array(
			'method'	=> $http_method,
			'data'		=> $formatted_data,
			'headers'	=> $headers
		);

		$payload = HTTPRequestService::performResourceRequest($http_options, $url, $config::IS_SSL_ENABLED, $access_token);

		if(!self::isValidJson($payload)) {
			// If it's a success, but it returns HTML, we want to let the user know.
			if(preg_match('/<h1>([^<\\/h1>]*)<\\/h1>/', $payload, $match)) {
				throw new OAuth2Exception("<em>The request was an apparent success, but a message was sent back: ".strip_tags($match[0])."</em>");
			}
		}

		$dataPayload = json_decode($payload);

		if(is_object($dataPayload)) {
			if (property_exists($dataPayload, 'error')) {
				// If the token is expired, OAuth will let us know.
				if ('invalid_token' == $dataPayload->error) {
					// Get another access token
					$this->makeRequest('api', 'GET', $url, $data);
				} else {
					if (is_object($dataPayload->error)) {
						throw new OAuth2Exception($dataPayload->error->message);
					} else {
						throw new OAuth2Exception($dataPayload->error.". ".$dataPayload->error_description);
					}
				}
			}
		}

		return $dataPayload;
	}

	/**
	 * Check the state of the token expiration as well as the ability to get the token via refresh token
	 *
	 * @param $authCode
	 *
	 * @return mixed
	 */
	public function checkAuthenticationState($authCode) {
		if (null === $this->getGrant()->getAccessToken()) {
            $this->getGrant()->authenticate();
		}
		
		$refreshed_access_token = $this->getGrant()->refreshTokens()->getAccessToken();
		$stored_access_token 	= $this->getGrant()->getAccessToken();
		$best_by_date 			= $this->getGrant()->getTimeToExpire();

		//check every time we perform an api request if the time_to_expire is less than now
		if (null !== $stored_access_token && time() < $best_by_date) {
			//if it's not return current access token
			$access_token = $stored_access_token;
		} else {
			// In the Implicit Grant Flow and Authorization Code Grant flows, there are redirects that deconstruct the
			// OAuth2Connection object. So in those cases, we will want to just pass it in.
			if (null !== $refreshed_access_token) {
				$access_token = $refreshed_access_token;
			} else {
				if (null === $authCode) {
					$access_token = $this->getGrant()->refreshTokens()->getAccessToken();
				} else {
					$access_token = $authCode;
				}
			}
		}

		return $access_token;
	}

	/**
	 * @param      $request_type
	 * @param      $http_method
	 * @param      $url
	 * @param      $data
	 * @param null $authCode
	 *
	 * @return mixed
	 * @throws OAuth2Exception
	 */
	public function makeRequest($request_type, $http_method, $url, $data = null, $authCode = null) {
		$access_token = $this->checkAuthenticationState($authCode);

		switch ($request_type) {
			case 'api':
				$dataPayload = $this->requestResource($access_token, $url, $http_method, $data);
				break;
			case 'oauth':
				$dataPayload = $this->requestResource($access_token, $url, $http_method, $data);
				break;
			default:
				throw new OAuth2Exception("Must be a request type of either 'oauth' or 'api'. Caller entered \"".$request_type."\", which is invalid.");
		}

		return $dataPayload;
	}

	/**
	 * @return OAuth2Flow
	 */
	public function getGrant() {
		return $this->grant;
	}

	/**
	 * @param OAuth2Flow $grant
	 */
	public function setGrantAndAuthenticate($grant) {
		$grant->authenticate();

		$this->grant = $grant;
	}

	/**
	 * @return OAuth2Config
	 */
	public function getConfig() {
		return $this->config;
	}

	/**
	 * @param OAuth2Config $config
	 */
	public function setConfig($config) {
		$this->config = $config;
	}
}