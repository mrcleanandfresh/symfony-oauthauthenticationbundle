<?php
/**
 * OAuth1Test.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */


namespace Tests\Authentication\OAuth\OAuth1;

use OAuth\AuthenticationBundle\Version\OAuth1\OAuth1;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OAuth1Test extends WebTestCase
{
	/**
	 * @var $OAuth1 OAuth1
	 */
	private $OAuth1;

	public function setUp()
	{
		$consumer_key = 'VXD22AD9kcNyNgsfW6cwkWRkw';
		$consumer_secret = 'y0k3z9Y46V0DMAKGe4Az2aDtqNt9aXjg3ssCMCldUheGBT0YL9';
		$token = '3232926711-kvMvNK5mFJlUFzCdtw3ryuwZfhIbLJtPX9e8E3Y';
		$token_secret = 'EYrFp0lfNajBslYV3WgAGmpHqYZvvNxP5uxxSq8Dbs1wa';

		$this->OAuth1 = new OAuth1($consumer_key, $consumer_secret, $token, $token_secret);
	}

	public function testCreateNonce()
	{
		$nonce = $this->OAuth1->createNonce();
		$this->assertTrue(!empty($nonce));
	}

	public function testCreateTimeStamp()
	{
		$timeStamp = $this->OAuth1->createTimeStamp();
		$this->assertTrue(!empty($timeStamp));
	}

	public function testBuildSignatureBaseStringWithPost()
	{
		$OAuth1 = $this->OAuth1;
		$nonce = $OAuth1::createNonce();
		$timestamp = $OAuth1::createTimeStamp();

		$request_type = null; //Test default of POST
		$url = "https://api.twitter.com/1.1/statuses/mentions_timeline.json";

		$signature_parameters = array(
			'oauth_consumer_key' 	=> 'VXD22AD9kcNyNgsfW6cwkWRkw',
			'oauth_nonce' 			=> $nonce,
			'oauth_signature_method' => $OAuth1::$signature_method,
			'oauth_timestamp' 		=> $timestamp,
			'oauth_token' 			=> '3232926711-kvMvNK5mFJlUFzCdtw3ryuwZfhIbLJtPX9e8E3Y',
			'oauth_version' 		=> $OAuth1::$version
		);

		$data = $OAuth1->buildSignatureBaseString($request_type, $url, $signature_parameters);
		$expected = "POST&https%3A%2F%2Fapi.twitter.com%2F1.1%2Fstatuses%2Fmentions_timeline.json&oauth_consumer_key%3DVXD22AD9kcNyNgsfW6cwkWRkw%26oauth_nonce%3D".rawurlencode($nonce)."%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D".rawurldecode($timestamp)."%26oauth_token%3D3232926711-kvMvNK5mFJlUFzCdtw3ryuwZfhIbLJtPX9e8E3Y%26oauth_version%3D1.0";

		$this->assertEquals($expected, $data);
	}

	public function testBuildSignatureBaseStringWithPostCaseOdd()
	{
		$OAuth1 = $this->OAuth1;
		$nonce = $OAuth1::createNonce();
		$timestamp = $OAuth1::createTimeStamp();

		$request_type = "PoSt";
		$url = "https://api.twitter.com/1.1/statuses/mentions_timeline.json";

		$signature_parameters = array(
			'oauth_consumer_key' 	=> 'VXD22AD9kcNyNgsfW6cwkWRkw',
			'oauth_nonce' 			=> $nonce,
			'oauth_signature_method' => $OAuth1::$signature_method,
			'oauth_timestamp' 		=> $timestamp,
			'oauth_token' 			=> '3232926711-kvMvNK5mFJlUFzCdtw3ryuwZfhIbLJtPX9e8E3Y',
			'oauth_version' 		=> $OAuth1::$version
		);

		$data = $OAuth1->buildSignatureBaseString($request_type, $url, $signature_parameters);
		$expected = "POST&https%3A%2F%2Fapi.twitter.com%2F1.1%2Fstatuses%2Fmentions_timeline.json&oauth_consumer_key%3DVXD22AD9kcNyNgsfW6cwkWRkw%26oauth_nonce%3D".rawurlencode($nonce)."%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D".rawurldecode($timestamp)."%26oauth_token%3D3232926711-kvMvNK5mFJlUFzCdtw3ryuwZfhIbLJtPX9e8E3Y%26oauth_version%3D1.0";

		$this->assertEquals($expected, $data);
	}

	public function testBuildSignatureBaseStringWithGetCaseOdd()
	{
		$OAuth1 = $this->OAuth1;
		$nonce = $OAuth1::createNonce();
		$timestamp = $OAuth1::createTimeStamp();

		$request_type = "GeT";
		$url = "https://api.twitter.com/1.1/statuses/mentions_timeline.json";

		$signature_parameters = array(
			'oauth_consumer_key' 	=> 'VXD22AD9kcNyNgsfW6cwkWRkw',
			'oauth_nonce' 			=> $nonce,
			'oauth_signature_method' => $OAuth1::$signature_method,
			'oauth_timestamp' 		=> $timestamp,
			'oauth_token' 			=> '3232926711-kvMvNK5mFJlUFzCdtw3ryuwZfhIbLJtPX9e8E3Y',
			'oauth_version' 		=> $OAuth1::$version
		);

		$data = $OAuth1->buildSignatureBaseString($request_type, $url, $signature_parameters);
		$expected = "GET&https%3A%2F%2Fapi.twitter.com%2F1.1%2Fstatuses%2Fmentions_timeline.json&oauth_consumer_key%3DVXD22AD9kcNyNgsfW6cwkWRkw%26oauth_nonce%3D".rawurlencode($nonce)."%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D".rawurldecode($timestamp)."%26oauth_token%3D3232926711-kvMvNK5mFJlUFzCdtw3ryuwZfhIbLJtPX9e8E3Y%26oauth_version%3D1.0";

		$this->assertEquals($expected, $data);
	}

	public function testBuildSignatureBaseStringWithGet()
	{
		$OAuth1 = $this->OAuth1;
		$nonce = $OAuth1::createNonce();
		$timestamp = $OAuth1::createTimeStamp();

		$request_type = "GET";
		$url = "https://api.twitter.com/1.1/statuses/mentions_timeline.json";

		$signature_parameters = array(
			'oauth_consumer_key' 	=> 'VXD22AD9kcNyNgsfW6cwkWRkw',
			'oauth_nonce' 			=> $nonce,
			'oauth_signature_method' => $OAuth1::$signature_method,
			'oauth_timestamp' 		=> $timestamp,
			'oauth_token' 			=> '3232926711-kvMvNK5mFJlUFzCdtw3ryuwZfhIbLJtPX9e8E3Y',
			'oauth_version' 		=> $OAuth1::$version
		);

		$data = $OAuth1->buildSignatureBaseString($request_type, $url, $signature_parameters);
		$expected = "GET&https%3A%2F%2Fapi.twitter.com%2F1.1%2Fstatuses%2Fmentions_timeline.json&oauth_consumer_key%3DVXD22AD9kcNyNgsfW6cwkWRkw%26oauth_nonce%3D".rawurlencode($nonce)."%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D".rawurldecode($timestamp)."%26oauth_token%3D3232926711-kvMvNK5mFJlUFzCdtw3ryuwZfhIbLJtPX9e8E3Y%26oauth_version%3D1.0";

		$this->assertEquals($expected, $data);
	}

	// Kind of a weak test, but the signature always changes except the equal sign.
	public function testSignature()
	{
		$OAuth1 = $this->OAuth1;
		$nonce = $OAuth1::createNonce();
		$timestamp = $OAuth1::createTimeStamp();

		$request_type = "GET";
		$url = "https://api.twitter.com/1.1/statuses/mentions_timeline.json";

		$query_params = array(
			'username'	=> 'intellidatainc',
			'status'	=> 'The Best Status'
		);

		$data = $OAuth1->signature($nonce, $timestamp, $request_type, $url, $query_params);
		$this->assertContains('=', $data);
	}

	public function testBuildOAuthHeader()
	{
		$OAuth1 = $this->OAuth1;
		$nonce = $OAuth1::createNonce();
		$timestamp = $OAuth1::createTimeStamp();

		$request_type = "GET";
		$url = "https://api.twitter.com/1.1/statuses/mentions_timeline.json";


		$query = array(
			'username'	=> 'intellidatainc',
			'status'	=> 'The Best Status'
		);

		$data = $OAuth1->buildOAuthHeader($request_type, $url, $query);

		$this->assertContains("Authorization: OAuth ", $data);
	}

	public function testSortParametersAlphaByKey()
	{
		$OAuth1 = $this->OAuth1;

		$testArray = array(
			'C'	=> 3,
			'BB' => 5,
			'A'	=> 1,
			'CC' => 6,
			'B'	=> 2,
			'AA' => 4
		);

		$expected = array(
			'A'	=> 1,
			'B'	=> 2,
			'C'	=> 3,
			'AA' => 4,
			'BB' => 5,
			'CC' => 6
		);

		$data = $OAuth1->sortParametersAlphaByKey($testArray);

		$this->assertEquals($expected, $data);
	}

	public function testEncodeParameterKeys()
	{
		$OAuth1 = $this->OAuth1;

		$testArray = array(
			'C '	=> 3,
			'BB' => 5,
			'A'	=> 1,
			'CC' => 6,
			'B'	=> 2,
			'AA' => 4
		);

		$expected = array(
			'C%20'	=> 3,
			'BB' => 5,
			'A'	=> 1,
			'CC' => 6,
			'B'	=> 2,
			'AA' => 4
		);;

		$data = $OAuth1->encodeParameterKeys($testArray);

		$this->assertEquals($expected, $data);
	}

	/**
	 * @expectedException \OAuth\AuthenticationBundle\Version\Excep\OAuth1Exception
	 */
	public function testOAuth1Exception()
	{
		$OAuth1 = $this->OAuth1;
		$OAuth1::$signature_method = "Something Wrong";
		$nonce = $OAuth1::createNonce();
		$timestamp = $OAuth1::createTimeStamp();

		$request_type = "GET";
		$url = "https://api.twitter.com/1.1/statuses/mentions_timeline.json";

		$query_params = array(
			'username'	=> 'intellidatainc',
			'status'	=> 'The Best Status'
		);

		$data = $OAuth1->signature($nonce, $timestamp, $request_type, $url, $query_params);
	}
}