<?php
/**
 * OAuth2ConnectionTest.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

namespace Test\OAuth\AuthenticationBundle;


use OAuth\AuthenticationBundle\OAuth2Connection;
use OAuth\AuthenticationBundle\Version\Excep\OAuth2Exception;
use OAuth\AuthenticationBundle\Version\OAuth2\Grants\ResourceOwnerPassword;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Config;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Validator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OAuth2ConnectionTest extends WebTestCase 
{
	/**
	 * @var $OAuthConnection OAuth2Connection
	 */
	private $OAuth2Connection;

	public function setUp() {
		$client_id = '3c1f5ba462006ddbfe4bc8a11898dabd';
		$client_secret = 'd95147dcc0818d676675f07a564c749e7e353a428069ba07';
		$username = "Intelli_SleuthTesting_data16";
		$password = "uproar mater whiff surge muffin";

		$redirect_uri = null;
		$response_type = 'code';
		$authorization_url = null;
		$token_url = "http://localhost:8080/component-edge/oauth/token";

		$OAuthConfig = new OAuth2Config($client_id, $client_secret, $redirect_uri, $response_type, $authorization_url, $token_url);

		$OAuthResourceOwnerPassword = new ResourceOwnerPassword($OAuthConfig, new OAuth2Validator(), $username, $password);

		$this->OAuth2Connection = new OAuth2Connection($OAuthConfig, $OAuthResourceOwnerPassword);
	}

	/**
	 * @expectedException \OAuth\AuthenticationBundle\Version\Excep\OAuth2Exception
	 */
	public function testInvalidCredentials() {
		$client_id = '3c1f5ba462006ddbfe4bc8a11898dabd';
		$client_secret = 'd95147dcc0818d676675f07a564c749e7e353a428069ba07';
		$username = "Intelli_SleuthTesting_data16";
		$password = "uproar";

		$redirect_uri = null;
		$response_type = 'code';
		$authorization_url = null;
		$token_url = "http://localhost:8080/component-edge/oauth/token";

		$OAuthConfig = new OAuth2Config($client_id, $client_secret, $redirect_uri, $response_type, $authorization_url, $token_url);

		$OAuthResourceOwnerPassword = new ResourceOwnerPassword($OAuthConfig, new OAuth2Validator(), $username, $password);

		$OAuth2Connection = new OAuth2Connection($OAuthConfig, $OAuthResourceOwnerPassword);

		$this->assertEquals('invalid_grant. Bad credentials.', $OAuth2Connection, 'invalid_grant. Bad credentials.');
	}

	public function testQueryData()
	{
		return array(
			'post'		=> true,
			'status'	=> "publish",
			'title'		=> "Best Day Ever",
			'content'	=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit neque orci, vel volutpat sem tincidunt vitae. Sed in neque sit amet ex auctor vestibulum et at sapien.",
			'limitTo'	=> 5
		);
	}

	public function testPostData()
	{
		return array(
			'post'		=> true,
			'status'	=> "publish",
			'title'		=> "Best Day Ever",
			'content'	=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit neque orci, vel volutpat sem tincidunt vitae. Sed in neque sit amet ex auctor vestibulum et at sapien.",
			'limitTo'	=> 5
		);
	}

	/**
	 * Build a GET request from the query string test
	 */
	public function testSetGETParameters() {
		$query = $this->testQueryData();
		$queryParms = $this->OAuth2Connection->setGETParameters($query);
		$expected = "?access_token=".$this->OAuth2Connection->getGrant()->getAccessToken()."&post=1&status=publish&title=Best+Day+Ever&content=Lorem+ipsum+dolor+sit+amet%2C+consectetur+adipiscing+elit.+Quisque+blandit+neque+orci%2C+vel+volutpat+sem+tincidunt+vitae.+Sed+in+neque+sit+amet+ex+auctor+vestibulum+et+at+sapien.&limitTo=5";

		$this->assertContains($expected, $queryParms);
	}

	/**
	 * Build POST parameters test
	 */
	public function testSetPOSTParameters() {
		$request = $this->testPostData();

		$data = $this->OAuth2Connection->setPOSTParameters($request);
		$expected = array(
			"post" => 'true',
			"status" => "publish",
			"title" => "Best Day Ever",
			"content" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit neque orci, vel volutpat sem tincidunt vitae. Sed in neque sit amet ex auctor vestibulum et at sapien.",
			"limitTo" => 5,
		);

		$this->assertEquals($expected, $data);
	}

	
	public function testRequestResource() {
		$access_token = $this->OAuth2Connection->getGrant()->getAccessToken();
		$url = "http://localhost:8080/component-edge/ce/api/definitions";
		$http_method = "gET";
		$data = null;
		
		$response = $this->OAuth2Connection->requestResource($access_token, $url, $http_method, $data);

		if(is_object($response)) {
			$this->assertTrue(!property_exists($response, 'error'));
			$this->assertContainsOnly('object', $response);
		} else {
			$this->assertContainsOnly('object', $response);
		}
	}


	/**
	 * @expectedException \OAuth\AuthenticationBundle\Version\Excep\OAuth2Exception
	 */
	public function testRequestResourceInvalid() {
		$access_token = $this->OAuth2Connection->getGrant()->getAccessToken();
		$url = "http://localhost:8080/component-edge/ce/api/path/to/invalid/resources";
		$http_method = "GEt";
		$data = null;

		$this->OAuth2Connection->requestResource($access_token, $url, $http_method, $data);
	}

	/*
	 * Testing an invalid Access Token (GET)
	 *
	 * Since the request is a GET method, the access token is taken from the Grant Object's properties:
	 * 		Example: "?access_token=" . $this->getGrant()->getAccessToken() . "&" . http_build_query($query_array);
	 * So we need to set it to null before sending the request.
	 */
	public function testRequestResourceInvalidAccessToken() {
		$this->OAuth2Connection->getGrant()->setAccessToken("5c9a5726");
		$access_token = $this->OAuth2Connection->getGrant()->getAccessToken();
		$url = "http://localhost:8080/component-edge/ce/api/definitions";
		$http_method = "GeT";
		$data = null;

		$response = $this->OAuth2Connection->requestResource($access_token, $url, $http_method, $data);

		$this->assertObjectHasAttribute('error', $response);
	}

	/**
	 * @expectedException \Exception
	 */
	public function testRequestResourceInvalidNullAccessToken() {
		$access_token = null;
		$url = "http://localhost:8080/component-edge/ce/api/path/to/invalid/resources";
		$http_method = "geT";
		$data = null;

		$this->OAuth2Connection->requestResource($access_token, $url, $http_method, $data);
	}

	public function testCheckAuthenticationState() {
		$access_token = $this->OAuth2Connection->getGrant()->getAccessToken();

		$eval_access_token = $this->OAuth2Connection->checkAuthenticationState($access_token);

		$this->assertNotNull($eval_access_token);
	}
	
	public function testCheckAuthenticationStateWithNullAccessToken() {
		$this->OAuth2Connection->getGrant()->setAccessToken(null);

		$eval_access_token = $this->OAuth2Connection->checkAuthenticationState(null);

		$this->assertNotNull($eval_access_token);
	}

	public function testCheckAuthenticationStateWithNullAuthToken() {
		$this->OAuth2Connection->getGrant()->setTimeToExpire(time() - 200);
		$eval_access_token = $this->OAuth2Connection->checkAuthenticationState(null);

		$this->assertNotNull($eval_access_token);
	}

	public function testMakeRequestUsingAPI() {
		$response = $this->OAuth2Connection->makeRequest('api', 'GET', 'http://localhost:8080/component-edge/ce/api/definitions');

		if(is_object($response)) {
			$this->assertTrue(!property_exists($response, 'error'));
			$this->assertContainsOnly('object', $response);
		} else {
			$this->assertContainsOnly('object', $response);
		}
	}

	public function testMakeRequestUsingOAuth() {
		$response = $this->OAuth2Connection->makeRequest('oauth', 'GET', 'http://localhost:8080/component-edge/ce/api/definitions');

		if(is_object($response)) {
			$this->assertTrue(!property_exists($response, 'error'));
			$this->assertContainsOnly('object', $response);
		} else {
			$this->assertContainsOnly('object', $response);
		}
	}

	/**
	 * @expectedException \OAuth\AuthenticationBundle\Version\Excep\OAuth2Exception
	 */
	public function testMakeRequestUsingInvalid() {
		$response = $this->OAuth2Connection->makeRequest('invalid', 'GET', 'http://localhost:8080/component-edge/ce/api/definitions');

		if(is_object($response)) {
			$this->assertTrue(!property_exists($response, 'error'));
			$this->assertContainsOnly('object', $response);
		} else {
			$this->assertContainsOnly('object', $response);
		}
	}
}