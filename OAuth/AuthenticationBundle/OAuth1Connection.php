<?php
/**
 * OAuth1Connection.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

namespace OAuth\AuthenticationBundle;

use OAuth\AuthenticationBundle\Impl\RestAPI;
use OAuth\AuthenticationBundle\Version\Excep\OAuth1Exception;
use OAuth\AuthenticationBundle\Version\OAuth1\OAuth1;

/**
 * Class OAuth1Connection
 * @package Authentication
 */
class OAuth1Connection extends OAuth1 implements RestAPI
{
	/**
	 * OAuth1Connection constructor.
	 *
	 * @param $consumer_key
	 * @param $consumer_secret
	 * @param $token
	 * @param $token_secret
	 */
	public function __construct($consumer_key, $consumer_secret, $token, $token_secret)
	{
		parent::__construct($consumer_key, $consumer_secret, $token, $token_secret);
	}

	/**
	 * @param $query
	 *
	 * @return string
	 * @throws OAuth1Exception
	 */
	function setGETParameters($query) {
		if(!empty($query)) {
			$query_array = array();
			foreach ($query as $parameter => $value)
			{
				if ($parameter !== '')
				{
					$query_array[$parameter] = $value;
				}
			}

			error_log("?" . http_build_query($query_array));
			return "?" . http_build_query($query_array);
		} else {
			throw new OAuth1Exception("The Query was empty.");
		}
	}

	/**
	 * @param $request
	 *
	 * @return mixed
	 */
	function setPOSTParameters($request) {
		// Essentially, if a value is a boolean, we want to change it to a string.
		foreach ($request as $key => &$value)
		{
			if (is_bool($value))
			{
				$value = ($value === true) ? 'true' : 'false';
			}
		}
		return $request;
	}

	/**
	 * @param $string
	 *
	 * @return bool
	 */
	static function isValidJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	/**
	 * @param $url
	 * @param $http_method
	 * @param $data
	 *
	 * @return mixed
	 * @throws OAuth1Exception
	 */
	function requestResource($url, $http_method, $data) {
		$header = $this->buildOAuthHeader($http_method, $url, $data);

		$options = array(
			CURLOPT_HTTPHEADER => array($header),
			CURLOPT_HTTPGET => true,
			CURLOPT_HEADER => false,
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_TIMEOUT => 10
		);

		if( "GET" === strtoupper($http_method)) {

			$options[CURLOPT_URL] .= '?' . http_build_query($data);

		} elseif ( "POST" === strtoupper($http_method) ) {

			$options[CURLOPT_POSTFIELDS] = http_build_query($data);

		}

		$ch = curl_init();
		curl_setopt_array( $ch , $options );
		$json = curl_exec($ch);

		if (($error = curl_error($ch)) !== '')
		{
			curl_close($ch);

			throw new OAuth1Exception($error);
		}

		curl_close($ch);

		return $json;
	}
}